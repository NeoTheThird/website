#!/bin/bash
# npm i -g prettier prettier-plugin-go-template
npx prettier "*|!(themes)/**" --ignore-path=.gitignore --arrow-parens=avoid --trailing-comma=none -uw
