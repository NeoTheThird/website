+++
authors = ["Marius Gripsgård", "Jan Sprinz"]
date = "2017-09-17"
title = "Ubuntu Touch is alive!"
subtitle = "Meet the UBports Community"
conference = "Ubucon Europe"
city = "Paris"
description = "A quick glance at the ragtag group of volunteers who one day decided to fork a mobile operating system"
[[links]]
  label = "ODP"
  link = "https://github.com/NeoTheThird/talks/blob/master/ubucon-2017-english.odp"
[[links]]
  label = "Speakerdeck"
  link = "https://speakerdeck.com/neothethird/ubuntu-touch-is-alive-meet-the-ubports-community"
+++
