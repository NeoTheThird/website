+++
authors = ["Jan Sprinz"]
date = "2019-12-28"
title = "Exciting developments around Linux on Phones!"
subtitle = "Ubuntu Touch, Plasma Mobile, Halium, PinePhone, and VollaPhone"
conference = "36th Chaos Communication Congress (36c3)"
city = "Leipzig"
description = "There have been countless attempts to dethrone Android. Many tried, many failed. But 2020 might be the year we finally see this change!"
mp4 = "https://cdn.media.ccc.de/congress/2019/h264-hd/36c3-10524-eng-deu-fra-Lightning_Talks_Day_2_hd.mp4#t=4909,5211"
mp3 = "https://cdn.media.ccc.de/congress/2019/mp3/36c3-10524-eng-Lightning_Talks_Day_2_mp3.mp3#t=4909,5211"
ogg = "https://cdn.media.ccc.de/congress/2019/opus/36c3-10524-eng-Lightning_Talks_Day_2_opus.opus#t=4909,5211"
[[links]]
  label = "beamer"
  link = "https://github.com/NeoTheThird/talks/blob/master/36c3-english.md"
[[links]]
  label = "Speakerdeck"
  link = "https://speakerdeck.com/neothethird/exciting-developments-around-linux-on-phones"
[[links]]
  label = "Schedule"
  link = "https://events.ccc.de/congress/2019/wiki/index.php/Static:Lightning_Talks#Day_2_-_2019-12-28"
+++
