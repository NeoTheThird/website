+++
authors = ["Jan Sprinz"]
date = "2019-10-12"
title = "State of the Touch - Ubuntu on phones and tablets"
subtitle = "A deep dive into the past, present, and future of Ubuntu Touch"
conference = "Ubucon Europe"
city = "Sintra"
description = "Ubuntu Touch is the mobile version of the Ubuntu operating system. The project was started by Canonical in 2015 and saw great interest in the community from the beginning. In 2017, as part of Canonical's corporate restructuring, ownership of the operating system was transferred to the UBports Community, a volunteer-led group of developers who had previously worked on bringing Ubuntu Touch to third-party Android devices."
youtube = "onpMz890XmU"
featured_image = "/images/sintra.jpeg"
featured = true
[[links]]
  label = "ODP"
  link = "https://github.com/NeoTheThird/talks/blob/master/ubucon-2019-english.odp"
[[links]]
  label = "Speakerdeck"
  link = "https://speakerdeck.com/neothethird/state-of-the-touch-ubuntu-on-phones-and-tablets"
[[links]]
  label = "Schedule"
  link = "https://manage.ubucon.org/eu2019/talk/JU88PN/"
+++
