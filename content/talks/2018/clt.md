+++
authors = ["Jan Sprinz"]
date = "2018-03-11"
title = "Ubuntu Touch"
subtitle = "Der Stand von GNU/Linux auf mobilen Geräten"
conference = "Chemnitzer Linux Tage"
city = "Chemnitz"
description = "Ubuntu Touch ist eine Version des Ubuntu-Betriebssystems für Smartphones und Tablets. Das Projekt wurde 2017 offiziell von Canonical eingestellt und wird seitdem von der UBports Community weitergeführt. Der Referent gibt einen Ausblick auf die Geschichte von Ubuntu Touch und berichtet von den Erfahrungen, die das Team bei der Übernahme des Projekts gemacht hat."
original_link = "https://chemnitzer.linux-tage.de/2018/de/programm/beitrag/289"
mp4 = "https://chemnitzer.linux-tage.de/2018/media/vortraege/video/So_V2_1500_1.mp4"
mp3 = "https://chemnitzer.linux-tage.de/2018/media/vortraege/audio/so_v2_1500.mp3"
ogg = "https://chemnitzer.linux-tage.de/2018/media/vortraege/audio/so_v2_1500.ogg"
[[links]]
  label = "ODP"
  link = "https://github.com/NeoTheThird/talks/blob/master/clt-2018-german.odp"
[[links]]
  label = "Speakerdeck"
  link = "https://speakerdeck.com/neothethird/linux-auf-mobilen-geraten"
+++
