+++
date = "2022-04-28"
title = "Passing on the torch at the UBports Foundation Board of Directors"
description = ""
math = false
tags = ["UBports"]
categories = []
series = []
#featured_image = ""
+++

After five years on the [UBports Foundation](https://ubports.com) Board of Directors, it was time for me to make room for fresh faces and not to run for re-election.

I've been around since the first days of planning the Foundation, back when we were just a small porting community. Canonical handing over the project to us changed everything. For a long time, [Ubuntu Touch](https://ubuntu-touch.io) has been my life.

It's been an amazing ride, and I'm incredibly grateful for the trust placed in me. But I'm now glad to have so many great community members who will step up and continue the work.

Godspeed and all the best to the newly elected leadership team! Looking forward to watching y'all take this community to new heights.

I will stay around tho, don't worry! I still believe in the vision, and you all know I have big plans for the [UBports Installer](https://github.com/ubports/ubports-installer) and [OPEN-CUTS](https://ubports.open-cuts.org/).

Looking forward!
