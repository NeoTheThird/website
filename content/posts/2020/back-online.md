+++
date = "2020-03-11"
title = "Back online!"
description = "Not much to see here yet..."
math = true
tags = ["hugo"]
categories = []
series = []
+++

Welcome to my blog about life, the universe, and everything! I'm not promising to write here very often, but a couple [Ubuntu Touch](https://ubuntu-touch.io) stories from time to time might be in the cards ;)

Finally got around to sit down for a couple minutes and set this up. It's served by [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) and built using the [Hugo-Coder](https://themes.gohugo.io/hugo-coder/) theme. Look, it even supports $\LaTeX$ and `code`! Nifty!
