+++
date = "2021-09-16"
authors = ["Johannah Sprinz"]
title = "A brief Introduction to First-Order Logic Unification at the example of Corbin and Bidoit's variation of Robinson's Unification Algorithm"
publisher = "LMU Munich, Master Seminar “Unification” SS21"
city = "Munich"
original_link = "https://www.cip.ifi.lmu.de/~sprinz/sprinz_2021_unification.pdf"
pdf = "https://www.cip.ifi.lmu.de/~sprinz/sprinz_2021_unification.pdf"
series = ["Unification"]
[identifiers]
  fatcat_work = "7ub52kkpvvdlfgbw7d3jo5mn5e"
  fatcat_release = "b2graaepdfhn7ayluo45gholyu"
[[links]]
  label = "ResearchGate"
  link = "https://www.researchgate.net/publication/358249030_A_brief_Introduction_to_First-Order_Logic_Unification_at_the_example_of_Corbin_and_Bidoit's_variation_of_Robinson's_Unification_Algorithm"
+++

Automated reasoning and deduction have interested computer scientists and logicians since the earliest days of modern information technology. One of the fundamental processes of this field is unification, meaning the automated solving of equations containing symbolic expressions. This paper revisits the 1965 Robinson Unification algorithm, which is now recognized as being the first machine-oriented unification algorithm for first-order logic. The primary criticism to be brought against this algorithm is its exponential space complexity. Corbin and Bidoit proposed a revised version of the algorithm that aims to address this problem in 1983, which is discussed here as well.
