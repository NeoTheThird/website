+++
date = "2020-02-05"
authors = ["Jan Sprinz"]
title = "Public Key Cryptography"
publisher = "LMU Munich, Bachelor Seminar “Ausgewählte Kapitel der Informatik” WS19/20"
city = "München"
original_link = "https://www.cip.ifi.lmu.de/~sprinz/sprinz_2020_public-public-key-cryptography.pdf"
pdf = "https://www.cip.ifi.lmu.de/~sprinz/sprinz_2020_public-public-key-cryptography.pdf"
series = ["Bachelorseminar"]
+++

This paper provides a practical introduction to the field of cryptography, with a focus on public-key cipher systems. The key concepts of the field are explained using simple-to-understand real-world examples that demonstrate the problems different encryption systems set out to solve. Building on that, the RSA algorithm - a widely used public-key encryption system based on the factorization of large prime numbers - is introduced. After the mathematical concept has been established, the algorithm is again demonstrated using a simple example. The paper closes with a brief consideration of the security aspects of RSA and an outlook on the future.
