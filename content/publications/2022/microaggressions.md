+++
date = "2022-06-30"
authors = ["Johannah Sprinz"]
title = "\"Y'all are just too sensitive\""
subtitle = "A computational ethics approach to understanding how prejudice against marginalized communities becomes epistemic belief"
featured_image = "/images/ceth.png"
publisher = "arXiv"
preprint = true
city = "Ithaca, NY"
original_link = "https://arxiv.org/abs/2207.01017"
pdf = "https://www.cip.ifi.lmu.de/~sprinz/sprinz_2022_computational_ethics_microaggressions.pdf"
#mp4 = ""
featured = true

[iframe]
  src="https://netlogoweb.org/web?https://neothethird.gitlab.io/ceth-seminar/model.nlogo"
  title="Model"

#series = []

[identifiers]
  doi = "10.48550/arXiv.2207.01017"
  #fatcat_work = ""
  #fatcat_release = ""
+++

Members of marginalized communities are often accused of being "too sensitive" when subjected to supposedly harmless acts of microaggression. This paper explores a simulated society consisting of marginalized and non-marginalized agents who interact and may, based on their individually held convictions, commit acts of microaggressions. Agents witnessing a microaggression might condone, ignore or condemn such microaggressions, thus potentially influencing a perpetrator's conviction. A [prototype model has been implemented in NetLogo](https://netlogoweb.org/web?https://neothethird.gitlab.io/ceth-seminar/model.nlogo), and possible applications are briefly discussed.
