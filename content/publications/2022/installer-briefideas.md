+++
date = "2022-02-05"
authors = ["Johannah Sprinz"]
title = "A collaborative effort to create a user-friendly installer for different mobile operating systems"
featured_image = "https://zenodo.org/record/6288520/files/installer.png?download=1"
publisher = "The Journal of Brief Ideas"
#city = ""
original_link = "https://beta.briefideas.org/ideas/3f22fbc27fb6dcb3c4c33603ef597f9b"
pdf = "https://www.cip.ifi.lmu.de/~sprinz/sprinz_2022_briefideas_installer_print.pdf"
series = ["OPEN-CUTS", "UBports Installer"]
featured = false
[identifiers]
  doi = "10.5281/zenodo.6288520"
  fatcat_work = "pjkcfopy4bhjlbqqggxj5ri43y"
  fatcat_release = "22gvdjaua5crdcxgc22d3k3vyy"
[[links]]
  label = "ResearchGate"
  link = "https://www.researchgate.net/publication/358863100_A_collaborative_effort_to_create_a_user-friendly_installer_for_different_mobile_operating_systems"
[[links]]
  label = "OpenAIRE"
  link = "https://explore.openaire.eu/search/publication?pid=10.5281/zenodo.6288520"
+++

Various alternative mobile operating systems are available, but installing them on third-party hardware is often too difficult for users unfamiliar with command-line tools [[1](https://spri.nz/talks/2019/36c3-lightning/)]. Since most community-based projects only have very few pre-installed devices commercially available, they will remain inaccessible to users who are not technically literate enough.

The [UBports Installer](https://github.com/ubports/ubports-installer#readme) addresses this problem for [Ubuntu Touch](https://ubuntu-touch.io/) by providing a cross-platform graphical application that bundles the command-line utilities required for installation [[2](http://www.pms.ifi.lmu.de/publikationen/#BA_Johannah.Sprinz), p. 21]. Device-specific installation instructions can be defined using a flexible YAML syntax [[3](https://github.com/ubports/installer-configs#v2)]. These files are editable through a public [GitHub repository](https://github.com/ubports/installer-configs) to enable community contributions. To allow developers to troubleshoot issues with devices they do not own themselves, users of the UBports Installer can automatically report installation results to a [logging server](https://ubports.open-cuts.org/system/5e9d746c6346e112514cfec7) [[2](http://www.pms.ifi.lmu.de/publikationen/#BA_Johannah.Sprinz), p. 57].

Initial experiments with adding other operating systems to the UBports Installer yielded promising results [[3](https://github.com/ubports/installer-configs#v2)], but further work is required to research and implement more installation procedures. Contributors from various open-source communities should be invited to contribute installation instructions. It might also be possible to automatically generate installation instructions from existing structured data, such as the [LineageOS wiki source files](https://github.com/LineageOS/lineage_wiki/tree/master/_data/devices).
