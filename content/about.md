+++
title = "About me"
+++

![Johannah Sprinz](/images/avatar.jpeg "Johannah Sprinz")

Hey there, I'm Johannah Sprinz, or Hannah for short. I study computer science at [LMU Munich](https://lmu.de). On this website, you can find some [talks](/talks) I have given at different tech and foss conferences and a few [scientific publications](/publications). You might also be interested in reading my [blog posts](/posts) or perusing my [media appearances](/media).

In my free time, I contribute to the development of the free and open-source mobile operating system [Ubuntu Touch](https://ubuntu-touch.io) and various other [Ubuntu](https://ubuntu.com)-related [projects](/projects). I'm also a co-founder of the [UBports Foundation](https://ubports.com) and the lead developer of the [UBports Installer](https://github.com/ubports/ubports-installer).

If you want to support my work, please consider donating to the [UBports Foundation](https://ubports.com/donate).

## FAQ

### Are you employed by the UBports Foundation?

No, I am not, nor have I ever been. I have, however, sometimes received financial support for conference attendances on behalf of the project. I have also received some development devices to enable my work related to Ubuntu Touch and the UBports Installer - from the Foundation directly, community members, or cooperation partners.

### I know you as $DEADNAME. Can I still call you that?

You might know me by my old name: Jan Sprinz. Don't worry; you've got the right person. I'm just not using that name anymore because I do not feel that it represents me. I go by Hannah or Johannah these days - either is fine. Thanks!

### \[insert insensitive question about my transition or trans people in general\]

Dude, read a book or something, I don't know. If we are good friends, we can talk about things. But, no offense, you really don't need to ask that question about my genitals that's on your mind right now. Here are some good resources to get you started:

- YouTube Essay [Identity: A Trans Coming Out Story](https://www.youtube.com/watch?v=AITRzvm0Xtg)
- Book [Serano, Julia (2007). Whipping Girl. ISBN: 9781580056236](https://www.juliaserano.com/whippinggirl.html)
- Documentary [Disclosure: Hollywood's view on Transgender](https://www.netflix.com/watch/81284247)
