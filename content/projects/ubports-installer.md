+++
title = "UBports Installer"
description = "user-friendly installation tool for alternative mobile operating systems"
featured_image = "/images/installer.png"
featured = true
github = "ubports/ubports-installer"
website = "https://devices.ubuntu-touch.io/installer/"
youtube = "zuLFTGkuD68"
[[links]]
  label = "Snap Store"
  link = "https://snapcraft.io/ubports-installer"
[[links]]
  label = "Device Configurations"
  link = "https://github.com/ubports/installer-configs"
+++

Installing UBports Ubuntu Touch on your device is easy! Just download the UBports Installer package for your operating system and sit back and relax while your computer does all the rest. Installing third-party operating systems has never been so easy!
